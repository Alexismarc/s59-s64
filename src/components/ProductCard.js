import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
  // Checks to see if the data was successfully passed
    // console.log(productProp);

    // console.log(productProp.name);
    /*console.log('this is from the ProductCard.js')
    console.log(productProp);*/

    const {_id, name, description, price} = productProp;
    // console.log(name);

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    const [count, setCount] = useState(0);

    const [stocks, setStocks] = useState(30);

    //we are goinbg to create a new state that will declare or tell the value of the disabled property in the button.
    const [isDisabled, setIsDisabled] = useState(false);

    // Function that keeps track of the Products
    // The setter function for UseState are asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed
    function product(){
        if (stocks > 1) {
            setCount(count + 1);
            // console.log('Product: ' + count);
            setStocks(stocks - 1);
            // console.log('Stocks: ' + stocks);
        } else {
            alert("Congratulations on getting the last product!");
            setStocks(stocks-1);
        };
    }

    //Define a 'useEffect' hook to have 'ProductCard' component do or perform a certain task after every changes in the stocks state
    //the side effect will run automatically in initial rendering and in every changes of the stocks state.
    //the array in the useEffect is called the dependency array
    useEffect(()=> {

        if(stocks === 0){
            setIsDisabled(true);
        }

    }, [stocks]);


    return (
    <Card>
        <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description:</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Card.Text>Stocks : {stocks}</Card.Text>
            <Button as= {Link} to = {`/products/${_id}`} variant="primary" disabled={isDisabled}>see more details</Button>
        </Card.Body>
    </Card>
      
  )
}

// Check if the ProductCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next
// ProductCard.propTypes = {
//     // The "shape" method is sued to check if a prop object conforms to a specific shape
//     productProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })
// }
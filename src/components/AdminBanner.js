/*
import { Button, Row, Col } from 'react-bootstrap';

export default function AdminBanner() {

	return (
		<Row>
            <Col className="p-5">
                <h1>Admin Dashboard</h1>
				<Button variant="primary">Add Product</Button>{''}

            </Col>
        </Row>


	);
}
*/
//----------------------------------------------------

import { useState } from 'react';
import { Button, Row, Col, Modal, Form } from 'react-bootstrap';

export default function AdminBanner({ handleCreateProduct }) {
  const [showModal, setShowModal] = useState(false);
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productPrice, setProductPrice] = useState('');

  const handleShowModal = () => setShowModal(true);
  const handleCloseModal = () => setShowModal(false);

  const handleCreateButtonClick = () => {
    handleCreateProduct({
      name: productName,
      description: productDescription,
      price: productPrice,
    });
    handleCloseModal();
  };

  return (
    <>
      <Row>
        <Col className="p-5">
          <h1>Admin Dashboard</h1>
          <Button variant="primary" onClick={handleShowModal}>
            Add Product
          </Button>
        </Col>
      </Row>

      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Create New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={productDescription}
                onChange={(e) => setProductDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                value={productPrice}
                onChange={(e) => setProductPrice(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Cancel
          </Button>
          <Button variant="primary" onClick={handleCreateButtonClick}>
            Create Product
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}


import { Row, Col, Card } from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';
import carouselImage1 from '../images/carousel1.png';
import carouselImage2 from '../images/carousel2.png';
import carouselImage3 from '../images/carousel3.png';



export default function Highlights() {
  return (
    <div>
      <h2 className="d-flex justify-content-center align-items-center mt-3 mb-3">Hair Importance</h2>
      <Row className="mt-3 mb-3">
        {/* Highlight Cards */}
        <Col xs={12} md={4}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>
                <h2>Social and Cultural Significance:</h2>
              </Card.Title>
              <Card.Text>
                Hair is an essential aspect of our appearance and plays a significant role in defining individuality and identity. Different hairstyles, colors, and lengths can reflect personal style and cultural expressions. In many cultures, hair can carry significant symbolic and religious meanings. For some, hair can be a source of pride and self-esteem.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>
                <h2>Fashion and Beauty:</h2>
              </Card.Title>
              <Card.Text>
                Hair is often considered a crucial component of beauty and fashion. Styling and maintaining hair can enhance a person's overall appearance and contribute to their sense of attractiveness. Many industries, such as hair care, beauty, and fashion, revolve around providing products and services related to hair.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>
                <h2>Communication and Expression:</h2>
              </Card.Title>
              <Card.Text>
                Hair can be used as a form of communication and self-expression. People often use hairstyles and hair colors to convey aspects of their personality, cultural background, or beliefs. It can be a way to stand out, make a statement, or be part of a particular group or community.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Carousel className="d-block mx-auto" style={{ maxWidth: '600px', maxHeight: '300px', marginBottom: '100px' }}>

        <Carousel.Item>
          <img
            className="d-block w-100"
            src={carouselImage1}
            alt="Second slide"
          />
          <Carousel.Caption>
            <h3>Enhanced Shine</h3>
            <p> It adds a beautiful shine to your hair, giving it a healthy and vibrant appearance.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={carouselImage2}
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3>Damage Reduction</h3>
            <p> The treatment can help minimize damage to your hair by providing a protective layer and reducing the need for excessive heat styling.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={carouselImage3}
            alt="Fourt slide"
          />
          <Carousel.Caption>
            <h3>Improved Manageability</h3>
            <p>The treatment makes your hair easier to style and maintain, reducing the time and effort needed for daily hair care.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
  );
}

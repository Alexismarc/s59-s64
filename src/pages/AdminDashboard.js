/*
import { Fragment } from 'react';
import AdminTable from '../components/AdminTable';
import AdminBanner from '../components/AdminBanner';

export default function AdminDashboard(){
	
	return (
		<Fragment>
			<AdminBanner />
			<AdminTable />
		</Fragment>
	)
}
*/

//----------------------------------------------------------

import { Fragment, useState, useEffect } from 'react';
import AdminTable from '../components/AdminTable';
import AdminBanner from '../components/AdminBanner';

export default function AdminDashboard() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => setProducts(data));
  };

  const handleCreateProduct = (newProduct) => {
    // Make the API call to add the new product to the database
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(newProduct),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          fetchProducts(); // Fetch the updated list of products from the server
          // Show success message using Swal2 or other notification library
        } else {
          // Show error message using Swal2 or other notification library
        }
      })
      .catch((error) => {
        // Handle errors
        console.error('Error creating product:', error);
        // Show error message using Swal2 or other notification library
      });
  };

  return (
    <Fragment>
      <AdminBanner handleCreateProduct={handleCreateProduct} />
      <AdminTable products={products} fetchProducts={fetchProducts} />
    </Fragment>
  );
}

